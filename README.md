## Smokeping on Docker —— 探针，一键部署
  
### 0. 一点废话
~~全新的dev分支已经加入肯德基豪华套餐~~    
最近新开发的版本，因为实验性质的东西太多，暂时加入dev分支，  
等到测试通过后，会转入stable通道！具体的使用方法都是一样的!   
镜像名称：**ilemonrain/smokeping:dev**   
  
此镜像仍然处于开发阶段！不建议大量用到生产环境中！    
  
如发现BUG，或者有更好的建议反馈，欢迎E-mail到：ilemonrain#ilemonrain.com  
  
### 1. 镜像说明  
听说Smokeping很难部署？  
  
刚开始的时候，我是不信的；  
  
部署起来发现，我靠，真TM难……  
  
还行谢谢谷歌娘，帮了我不少忙，  
  
现在，Smokeping成功Docker化，  
  
让探针部署不再那么难过登天。

镜像特点：
 - 内建Smokeping 2.7.1版本 (相对于目前已有的教程，此镜像所带的Smokeping可能为最新版本)  
 - Smokeping、fping、echoping组件均为编译安装，保证版本最新以及安全  
 - 集成SSH能力 ! SSH登录，尽情的魔改搞大新闻!  
 - 带有面板访问密码保护 (可SSH登录后自行再配置)  
 - Docker部署，避免对系统环境的破坏，所有的Smokeping操作都会被限制在Docker容器内；删除Docker容器即意味着完美移除环境 !

### 2. 部署方法  
启动命令行：  
```
docker run -d -p 22:22 -p 80:80 -p 443:443 -h hostname --name smokeping ilemonrain/smokeping 
```
  
参数说明：  
 > **-d：** 以后台模式启动  
 > **-p 22:22：** OpenSSH-Server (SSH) 使用的端口，可自行修改映射，比如 -p 3322:22，下面的端口映射均同此映射规则  
 > **-p 80:80:** Apache2.4-httpd (WebServer) 使用的HTTP端口  
 > **-p 443:443:** Apache2.4-httpd (WebServer) 使用的HTTPS端口  
 > **-h hostname:** 设置Docker容器的主机名 (Hostname)  
 > **--name smokeping:** 设置Docker容器名称 (docker ps可见)  
 > **ilemonrain/smokeping:** 镜像名称，勿动  
  
### 3. SSH使用方法  
使用任何SSH连接工具 (如SecureCRT、Xshell、Linux系统自带的SSH命令) 连接到 **你的服务器IP:SSH端口** ，例如 123.234.123.234:22 (如果是SSH链接的话，可能是这样的：ssh://123.234.123.234:22)，请使用以下凭据登录：
 > **用户名：** root  
 > **密码：** smokeping  

强烈建议在登录成功后，**立即修改掉你的Root密码**，使用命令：
> passwd root  

来修改你的ROOT密码！

### 4. Smokeping Web面板登录方法  
~~当你成功的启动Docker容器后，访问你的 服务器IP ，会弹出一个登录对话框，请使用以下凭据登录：~~  

太不方便了！取消凭据登录！开门见山！   
  
### 5. 命令映射(简化)  
为方便执行命令，我在制作这个镜像时，将组件的命令进行了软连接处理，以避免敲击过多的无用字符。下面的是映射对应关系，前面的是简化后的命令，后面的是其原始位置：  
 > **httpd** -> /usr/sbin/httpd  
 > **smokeping** -> /usr/local/sbin/smokeping -> /usr/local/smokeping/bin/smokeping  
 > **fping** -> /usr/local/sbin/fping  
 > **echoping** -> /usr/local/sbin/echoping -> /usr/local/echoping/bin/echoping  

### 6. 关于systemd(systemctl)
受到Docker CentOS 7的限制，在此镜像中，你将**无法使用systemd**，例如**systemctl**、**service**，请**尽可能规避这些命令**！  

### 7. 关于镜像开发状态
镜像目前仍然处于开发期，未经过大范围的应用测试，不建议大量部署到生产环境中！如有BUG反馈或者好的建议，欢迎联系我！

### 8. 联系作者
E-mail：ilemonrain#ilemonrain.com  
Telegram：@ilemonrain