#!/bin/bash
#
# ===== 开始构建镜像 =====
#
# 配置环境
export SYSTEM_ROOT_PASSWORD=smokeping
export SYSTEM_TIMEZONE=Asia/Shanghai
# 安装 epel-release
yum install -y epel-release
# 更新系统
yum update -y
# 安装基础组件
# 经过测试sendmail组件会导致出图不稳定，故去掉此组件
yum install -y wget
# 配置时区 (根据反馈，错误的时区会导致Smokeping绘图时间不准)
yum install -y tzdata
/bin/cp /usr/share/zoneinfo/$SYSTEM_TIMEZONE /etc/localtime && echo $SYSTEM_TIMEZONE > /etc/timezone
# 安装编译组件
yum install -y gcc make
# 安装Apache (Web服务器)
yum install -y httpd httpd-devel mod_ssl
# 安装OpenSSH-Server (SSH服务器)
yum install -y openssh-server
# 配置OpenSSH-Server
sed -i 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
sed -i 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/' /etc/ssh/sshd_config
mkdir -p /root/.ssh/
echo "StrictHostKeyChecking=no" > /root/.ssh/config
echo "UserKnownHostsFile=/dev/null" >> /root/.ssh/config
# 生成SSH密钥
ssh-keygen -A 
# 设置系统root密码
echo "root:${SYSTEM_ROOT_PASSWORD}" | chpasswd
# 编译安装 Fping
cd /build
tar xvf fping-4.0.tar.gz && cd fping-4.0
./configure && make && make install
ln -s /usr/local/sbin/fping /usr/local/bin/fping
ln -s /usr/local/sbin/fping /usr/sbin/fping
ln -s /usr/local/sbin/fping /usr/bin/fping
cd /build
rm -rf fping-4.0.tar.gz fping-4.0
# 编译安装 Echoping
cd /build
yum install -y popt-devel libidn-devel
tar xvf echoping-6.0.2.tar.gz && cd echoping-6.0.2
./configure && make && make install
ln -s /usr/local/bin/echoping /usr/local/sbin/echoping
ln -s /usr/local/bin/echoping /usr/sbin/echoping
ln -s /usr/local/bin/echoping /usr/bin/echoping
cd /build
rm -rf echoping-6.0.2.tar.gz echoping-6.0.2
# 编译安装 RRDtool
cd /build
yum install -y glibc-devel libpng-devel pango-devel libxml2-devel perl-ExtUtils-CBuilder perl-ExtUtils-MakeMaker
tar xvf rrdtool-1.7.0.tar.gz && cd rrdtool-1.7.0
./configure --prefix=/usr/local/rrdtool && make && make install
cp /usr/local/rrdtool/lib/perl/$(perl -v | grep -o 5.[0-9][0-9].[0-9])/x86_64-linux-thread-multi/RRDs.pm /usr/lib64/perl5/
cp /usr/local/rrdtool/lib/perl/$(perl -v | grep -o 5.[0-9][0-9].[0-9])/x86_64-linux-thread-multi/auto/RRDs/RRDs.so /usr/lib64/perl5/
ln -s /usr/local/rrdtool/bin/* /usr/local/bin/
ln -s /usr/local/rrdtool/bin/* /usr/sbin/
ln -s /usr/local/rrdtool/bin/* /usr/bin/
cd /build
rm -rf rrdtool-1.7.0.tar.gz rrdtool-1.7.0
# 编译安装 Smokeping
cd /build
yum install -y perl-LWP-Protocol-https perl-CPAN perl-Module-Build perl-Test-RequiresInternet perl-Test-Warn perl-Sys-Syslog openssl openssl-devel wqy-zenhei-fonts -y
tar xvf smokeping-2.7.2.tar.gz && cd smokeping-2.7.2
export PERL5LIB=/usr/local/smokeping/thirdparty/lib/perl5/
./configure --prefix=/usr/local/smokeping 
gmake && gmake install
ln -s /usr/local/smokeping/bin/* /usr/local/bin/
ln -s /usr/local/smokeping/bin/* /usr/sbin/
ln -s /usr/local/smokeping/bin/* /usr/bin/
cd /build
rm -rf smokeping-2.7.2.tar.gz smokeping-2.7.2
# 配置Smokeping
cd /usr/local/smokeping/
mkdir cache data var
touch /var/log/smokeping.log
chown apache:apache cache/ data/ var/ /var/log/smokeping.log
mv /usr/local/smokeping/htdocs/smokeping.fcgi.dist /usr/local/smokeping/htdocs/smokeping.fcgi
mv /usr/local/smokeping/etc/config.dist /usr/local/smokeping/etc/config
ln -s /usr/local/smokeping/htdocs/smokeping.fcgi /usr/local/smokeping/htdocs/smokeping.cgi
sed -i "/step     = 300/s/300/60/" /usr/local/smokeping/etc/config
sed -i "/pings    = 20/s/20/20/" /usr/local/smokeping/etc/config
sed -i "160i \'--font\'\, \"TITLE:20:WenQuanYi Zen Hei Mono\"\," /usr/local/smokeping/lib/Smokeping/Graphs.pm
sed -i "50i charset= utf-8 " /usr/local/smokeping/etc/config
sed -i "/owner    =/s/Peter Random/Smokeping/" /usr/local/smokeping/etc/config
sed -i "/contact  =/s/some@address.nowhere/webmaster@smokeping.com/" /usr/local/smokeping/etc/config
sed -i "/sendmail = /s/sendmail/#sendmail/" /usr/local/smokeping/etc/config
sed -i "/mailhost =/s/my.mail.host/mail.smokeping.com/" /usr/local/smokeping/etc/config
sed -i "/cgiurl   =/s/some.url/www.smokeping.com/" /usr/local/smokeping/etc/config
sed -i "/to = /s/alertee@address.somewhere/webmaster@smokeping.com/" /usr/local/smokeping/etc/config
sed -i "/from = /s/smokealert@company.xy/smokealert@smokeping.com/" /usr/local/smokeping/etc/config
sed -i "/range = /s/10h/8h/" /usr/local/smokeping/etc/config
sed -i "/smokeping_secrets.dist/s/smokeping_secrets.dist/smokeping_secrets/" /usr/local/smokeping/etc/config
sed -i "/menu = James/s/James/Localhost/" /usr/local/smokeping/etc/config
sed -i "/title =James/s/James/ 本地服务器 (127.0.0.1)/" /usr/local/smokeping/etc/config
sed -i "/host = james.address/s/james.address/127.0.0.1/" /usr/local/smokeping/etc/config
sed -i "/++ MultiHost/s/++/#++/" /usr/local/smokeping/etc/config
sed -i "/menu = Multihost/s/menu/#menu/" /usr/local/smokeping/etc/config
sed -i "/title = James and James/s/title/#title/" /usr/local/smokeping/etc/config
sed -i "/host = \/Test\/James/s/host/#host/" /usr/local/smokeping/etc/config
mv /usr/local/smokeping/etc/smokeping_secrets.dist /usr/local/smokeping/etc/smokeping_secrets
chmod 600 /usr/local/smokeping/etc/smokeping_secrets && chown apache:apache /usr/local/smokeping/etc/smokeping_secrets
sed -i "/smokemail.dist/s/smokemail.dist/smokemail/" /usr/local/smokeping/etc/config
mv /usr/local/smokeping/etc/smokemail.dist /usr/local/smokeping/etc/smokemail
sed -i "/tmail.dist/s/tmail.dist/tmail/" /usr/local/smokeping/etc/config
mv /usr/local/smokeping/etc/tmail.dist /usr/local/smokeping/etc/tmail
sed -i "/basepage.html/s/basepage.html.dist/basepage.html/" /usr/local/smokeping/etc/config
sed -i "/Here you will learn all about the latency of our network./s/Here you will learn all about the latency of our network./当你看到这个消息时，说明 Smokeping 已经部署成功。请修改配置文件(位于\/usr\/local\/smokeping\/etc\/config)，开始配置Smokeping服务器。/" /usr/local/smokeping/etc/config
mv /usr/local/smokeping/etc/basepage.html.dist /usr/local/smokeping/etc/basepage.html
sed -i "/SmokePing Latency Page for/s/SmokePing Latency Page for/Smokeping 网络质量监测工具 - /" /usr/local/smokeping/etc/basepage.html
sed -i "/Running on/s/Running on/Running on Docker \<a href\=\"https:\/\/hub.docker.com\/r\/ilemonrain\/smokeping\/\"\>ilemonrain\/smokeping/" /usr/local/smokeping/etc/basepage.html
rm -f /usr/local/smokeping/etc/smokeping_secrets
cat > /usr/local/smokeping/etc/smokeping_secrets <<EOF
smokeping-slave-1:smokepingslave
smokeping-slave-2:smokepingslave
smokeping-slave-3:smokepingslave
smokeping-slave-4:smokepingslave
smokeping-slave-5:smokepingslave
EOF
chown apache:apache /usr/local/smokeping/etc/smokeping_secrets
chmod 600 /usr/local/smokeping/etc/smokeping_secrets
cat > /usr/local/smokeping/etc/slavesecrets <<EOF
smokepingslave
EOF
chown apache:apache /usr/local/smokeping/etc/slavesecrets
chmod 600 /usr/local/smokeping/etc/slavesecrets
# Smokeping 汉化 (原创汉化，转载及二次开发请保留原作者信息)
sed -i "/Filter menu.../s/Filter menu.../搜索 .../" /usr/local/smokeping/lib/Smokeping.pm
sed -i "/Last 3 Hours/s/Last 3 Hours/最近3小时/" /usr/local/smokeping/etc/config
sed -i "/Last 30 Hours/s/Last 30 Hours/最近30小时/" /usr/local/smokeping/etc/config
sed -i "/Last 10 Days/s/Last 10 Days/最近10天/" /usr/local/smokeping/etc/config
sed -i "/Last 400 Days/s/Last 400 Days/最近400天/" /usr/local/smokeping/etc/config
sed -i "/title = /s/Network Latency Grapher/Smokeping 网络质量监测/" /usr/local/smokeping/etc/config
sed -i "/remark = /s/Welcome to the SmokePing website of xxx Company./欢迎使用 Smokeping 网络质量监测工具，/" /usr/local/smokeping/etc/config
sed -i "/SmokePing Latency Page for/s/SmokePing Latency Page for/Smokeping 网络质量监测工具 - /" /usr/local/smokeping/etc/basepage.html
sed -i "/Running on/s/Running on/Running on Docker \<a href\=\"https:\/\/hub.docker.com\/r\/ilemonrain\/smokeping\/\"\>ilemonrain\/smokeping/" /usr/local/smokeping/etc/basepage.html
# 配置Apache Web Server
cat > /etc/httpd/conf.d/httpd.conf <<EOF
Alias /css "/usr/local/smokeping/htdocs/css/"
Alias /js "/usr/local/smokeping/htdocs/js"
Alias /cache "/usr/local/smokeping/cache/"
Alias /cropper "/usr/local/smokeping/htdocs/cropper/"
Alias /smokeping.cgi "/usr/local/smokeping/htdocs/smokeping.cgi"
Alias /smokeping.fcgi "/usr/local/smokeping/htdocs/smokeping.fcgi"
Alias /smokeping/smokeping.cgi "/usr/local/smokeping/htdocs/smokeping.cgi"
Alias /smokeping/smokeping.fcgi "/usr/local/smokeping/htdocs/smokeping.fcgi"
Alias / "/usr/local/smokeping/htdocs/smokeping.cgi"
<Directory "/usr/local/smokeping">
AllowOverride All
Options All
AddHandler cgi-script .cgi .fcgi
Order allow,deny
Allow from all
Require all granted
DirectoryIndex smokeping.cgi
</Directory>
EOF
rm -f /etc/httpd/conf.d/welcome.conf
# 配置 entrypoint.sh
cat > /usr/local/sbin/entrypoint.sh <<EOF
#!/bin/bash
/usr/sbin/httpd
/usr/sbin/smokeping
/usr/sbin/sshd -D
EOF
# 权限修复
chown -R apache:apache /usr/local/smokeping/cache/
chown -R apache:apache /usr/local/smokeping/data/
chown -R apache:apache /usr/local/smokeping/var/
chown -R apache:apache /var/log/smokeping.log
chown apache:apache /usr/local/smokeping/etc/smokeping_secrets
chmod 600 /usr/local/smokeping/etc/smokeping_secrets
chown apache:apache /usr/local/smokeping/etc/slavesecrets
chmod 600 /usr/local/smokeping/etc/slavesecrets
chmod +x /usr/local/sbin/entrypoint.sh
# 环境清理，去除无用软件
cd /
rm -rf /build/
yum remove -y gcc make wget cpp
yum clean all
rm -rf /var/cache/yum